# `archive_gitops`

This script provides utilities for archiving Git repositories and deploying them to a target directory using a GitOps approach.

## Overview

The script downloads repository archives from a Git host API, extracts them to a temporary directory, then relocates the extracted content to the target deployment path.  Downloads are deployed using a FIFO (first-in, first-out) method, whereas backups of previous deployments (and the corresponding 'rollbacks') are swapped using a LIFO (last-in, first-out) method.

Deployments and rollbacks can be triggered via commands that search for and reorder the backed up directories.  This allows deploying the latest archive or rolling back to previous versions in a sequential manner.

Once all downloads have been deployed, a sort of 'slide-rule' is achieved where it is possible to rollback and un-rollback between different versions.


## Requirements

The following environment variables must be set in the `.env.sh` file:

- `REPO_URL` - URL of the Git host API
- `REPO_SUBFOLDER` (optional) - Subfolder path within repository
- `PROJECT_ID` - ID of project
- `PROJECT_NAMESPACE` - Namespace for temporary files
- `TARGET_DIR` - Target path for deployments
- `PERSONAL_ACCESS_TOKEN` - Authentication token


## Commands

### download and deploy

```sh
./archive_gitops download [COMMIT]
./archive_gitops deploy [COMMIT]
```

Downloads and deploys the repository archive to the target directory.  If a commit is specified, that archive is deployed.  Otherwise the latest is used.

## rollback and unrollback

```sh
./archive_gitops rollback
```

Rolls back the deployment to the previous backed up version in the target directory.
unrollback

```sh
./archive_gitops rollback
./archive_gitops unrollback
```

Undoes a previous rollback, restoring the most recent deployment.


## Implementation Details

Deployments and rollbacks are achieved by reordering the backed up directories before moving them.  This provides an audit log of deployment versions.

Temporary files and directories are cleaned up after usage to avoid clutter.  File operations use verbose output for visibility.

Let me know if any part of the implementation or usage is unclear!
